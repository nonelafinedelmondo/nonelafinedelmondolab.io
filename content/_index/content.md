+++
fragment = "content"
#disabled = true
date = "2020-05-18"
weight = 110
background = "light"

title = "Studio di Consulenza Familiare"
subtitle = "Dott.ssa Maria Luisa Orsi - Avv. Sergio Spina"
#title_align = "left" # Default is center, can be left, right or center
+++

La mediazione familiare consta di interventi volti a risolvere
problemi all’interno della famiglia: sia quelli relativi alla
separazione e divorzio sia le dispute e i conflitti che possono
determinarsi durante tutta la vita di una famiglia

La mediazione familiare è utile per raggiungere un risultato concreto:
un progetto di riorganizzazione delle relazioni e delle loro modalità,
superando le ostilità e .....
