+++
fragment = "embed"
#disabled = false
date = "2020-05-19"
weight = 200
background = "light"

#title = ""
subtitle = "Studio di Torino"
title_align = "left"

media_source = "https://goo.gl/maps/qDBETJvbmUWqsUNe7"
media = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2818.6399033621933!2d7.6570423151203295!3d45.05252876883944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47886d255b6d9859%3A0xad55304c91470fd8!2sCorso%20Carlo%20e%20Nello%20Rosselli%2C%2068%2C%2010129%20Torino%20TO!5e0!3m2!1sit!2sit!4v1581007969702!5m2!1sit!2sit" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>'
#ratio = "16by9" # 21by9, 16by9, 4by3, 1by1 - Default: 4by3
size = "50" # 25, 50, 75, 100 (percentage) - default: 75
+++
