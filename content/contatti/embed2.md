+++
fragment = "embed"
#disabled = false
date = "2020-05-19"
weight = 250
background = "light"

#title = ""
subtitle = "Studio di Avigliana"
title_align = "left"

media_source = "https://goo.gl/maps/3kYRuVoLanrkSskH7"
media = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2816.990884404753!2d7.398846751024807!3d45.08597797899568!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4788470fcf19c0c7%3A0xcef2edcd520a6460!2sCorso%20Torino%2C%20142%2C%2010051%20Avigliana%20TO!5e0!3m2!1sit!2sit!4v1589873927392!5m2!1sit!2sit" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'
#ratio = "16by9" # 21by9, 16by9, 4by3, 1by1 - Default: 4by3
size = "50" # 25, 50, 75, 100 (percentage) - default: 75
+++
