+++
fragment = "contact"
#disabled = true
date = "2020-05-19"
weight = 400
background = "light"
form_name = "defaultContact"

#title = ""
#subtitle = "Mandaci un messaggio"
title_align = "center"

#PostURL can be used with backends such as mailout from caddy
#post_url = "sergio.am.spina@gmail.com"
email = "sergio.am.spina@gmail.com"
button_text = "Invia"
#netlify = false

#Optional google captcha
#Won't be used if netlify is enabled
#[recaptcha]
#sitekey = ""

[message]
  success = "Grazie per averci contattato. Ti invieremo una risposta il prima possibile."
  error = "Errore nell'invio del messagio. Contattaci alla mail sergio.am.spina@gmail.com"

#Only defined fields are shown in contact form
[fields.name]
  text = "Nome e Cognome *"
  error = "Inserisci il tuo nome."

[fields.email]
  text = "Email *"
  error = "Inserisci il tuo indirizzo email."

#[fields.phone]
#text = "Your Phone *"
#error = "Please enter your phone number" # defaults to theme default

[fields.message]
  text = "Messaggio *"
  error = "Inserisci il messaggio."

#Optional hidden form fields
#Fields "page" and "site" will be autofilled
#[[fields.hidden]]
#name = "page"
#
#[[fields.hidden]]
#name = "someID"
#value = "example.com"
+++
