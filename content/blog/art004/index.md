+++
fragment = "content"
weight = 100
date = "2020-05-18"
title = "L'adozione monogenitoriale"
background = "light"
+++

Quello di "famiglia" non è un concetto esattamente definibile,
trattandosi di un concetto che muta di senso e di contenuto a seconda
delle varie culture umane e, all'interno di una stessa cultura, a
seconda dei tempi e delle circostanze. Non esiste un "modello" statico
di famiglia, ma ogni singola famiglia è un modello a sè stante, dove
il denominatore comune della gratuità della cura reciproca viene
declinato in tante forme diverse quanto sono diverse le vite e le
attitudini dei suoi singoli componenti.

L'ordinamento italiano riconosce la molteplicità dei "modelli" di
famiglia ed appresta una regolamentazione anche per i casi meno
comuni. Ad esempio in Italia è possibile che un single adotti un
bambino.

L'adozione monogenitoriale è regolata dall'art. 44 della legge 184 del
1983, che recita:

>1. I minori possono essere adottati anche quando non ricorrono le
>   condizioni di cui al comma 1 dell'articolo 7:
>
>a) da persone unite al minore da vincolo di parentela fino al sesto
>   grado o da preesistente rapporto stabile e duraturo, quando il minore
>   sia orfano di padre e di madre;
>
>b) ...
>
>c) quando il minore si trovi nelle condizioni indicate dall'articolo
>   3, comma 1, della legge 5 febbraio 1992, n. 104, e sia orfano di
>   padre e di madre;
>
>d) quando vi sia la constatata impossibilità di affidamento
>   preadottivo.
>
>2. ...
>
>3. Nei casi di cui alle lettere a), c), e d) del comma 1 l'adozione è
>   consentita, oltre che ai coniugi, anche a chi non è coniugato.

Dunque il non coniugato può adottare un minore quando quest'ultimo
sia : 1) orfano di padre e di madre e sussista un vincolo di parentela
tra l'adottante e l'adottato o un "<i>preesistente rapporto stabile e
duraturo</i>"; 2) oppure sia orfano di padre e di madre e presenti
"<i>una minorazione fisica, psichica o sensoriale, stabilizzata o
progressiva, che è causa di difficoltà di apprendimento, di relazione
o di integrazione lavorativa e tale da determinare un processo di
svantaggio sociale o di emarginazione.</i>" (art. 3 legge 104/92); 3)
oppure quando vi sia la "<i>constatata impossibilità di affidamento
preadottivo</i>"

L'art. 25 della legge 184/93 sull'adozione prevede altri due casi
residuali di adozione monogenitoriale: quando cioè l'adozione viene
chiesta da una coppia ma, durante l'affidamento preadottivo, i coniugi
si separano o uno dei due muoia. Dispone infatti l'art. 25 ai commi 4
e 5:

>4. Se uno dei coniugi muore o diviene incapace durante l'affidamento
>   preadottivo, l'adozione, nell'interesse del minore, può essere
>   ugualmente disposta ad istanza dell'altro coniuge nei confronti di
>   entrambi, con effetto, per il coniuge deceduto, dalla data della
>   morte.
>5. Se nel corso dell'affidamento preadottivo interviene
>   separazione tra i coniugi affidatari, l'adozione può essere
>   disposta nei confronti di uno solo o di entrambi, nell'esclusivo
>   interesse del minore, qualora il coniuge o i coniugi ne facciano
>   richiesta.  </blockquote>

Si tratta di situazioni rare, al punto che i pochi casi di adozione
monogenitoriale in Italia hanno fatto notizia su giornali a diffusione
nazionale. Ma è bene tenere presente che un genuino atto d'amore,
anche se proveniente da una donna o un uomo non sposati, possono
sempre trovare ascolto e accoglimento presso i tribunali italiani.
