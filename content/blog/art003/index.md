+++
fragment = "content"
weight = 100
date = "2020-05-15"
title = "Le trasformazioni nella vita familiare"
background = "light"
+++

Ogni famiglia attraversa una successone di fasi diverse fra loro che
scandiscono il suo percorso, definendo il suo ciclo di vita.

Ogni fase è caratterizzata da specifici compiti di sviluppo che
comportano una continua rielaborazione dei rapporti a livello di
coppia, delle relazioni genitori-figli e di quelle con la famiglia
d’origine.

Ogni membro appartenente alla famiglia, in ogni fase del ciclo di vita
familiare, è impegnato ad affrontare più compiti di sviluppo, perché
coinvolto trasversalmente in più relazioni.

La soluzione di questi compiti consente il passaggio alla fase successiva.

Lo sviluppo si realizza nel tempo ed è scandito da eventi “critici”
che innescano processi di trasformazione, necessari al passaggio da
una fase all’altra del ciclo di vita.

Ogni famiglia attraversa cicli che si ripetono, caratterizzati da fasi
di funzionamento e di adattamento.

I periodi di crisi sono intervallati da fasi in cui si cerca di
stabilire un nuovo equilibri che poi viene mantenuto (periodo di
relativa tranquillità) fino ad una crisi successiva.

La **CRISI FAMIGLIARE**, o **EVENTO CRITICO**, comporta difficoltà
perché è un periodo di scarsa organizzazione, in cui l’equilibrio
precedentemente raggiunto viene a mancare. In questa fase c’è una
spinta e un’attività di ricerca per il raggiungimento di un nuovo
livello organizzativo.

Il superamento della crisi dipende in gran misura dalle risorse di cui
la famiglia dispone e dal potere rigenerativo della stessa, per
ristabilirsi dalla disgregazione dovuta all’evento stressante.

#### LE FASI DEL CICLO DI VITA DELLA FAMIGLIA

##### PRIMA FASE: COSTITUZIONE DELLA COPPIA CONIUGALE

Nella costruzione della coppia coniugale non abbiamo la semplice
unione di due individui ma l’incontro di due storie di vita. La COPPIA
è composta da tre parti: io, tu, noi.

Queste parti sono a loro volta rappresentate da : lui, lei, il modello
di coppia e le aspettative di lui, il modello di coppia e le
aspettative di lei.

I modelli di aspettative di ogni soggetto sono il prodotto delle
attese elaborate dalle rispettiva famiglie d’origine.

Nella formazione una nuova coppia non si incontrano semplicemente i
due partecipanti alla coppia, ma due storie di vita vissuta con
altrettante storie e relazioni alle spalle.

Famiglie e credenze di lei, famiglia e credenze di lui.

Ogni membro porta all’interno della coppia attese costruite ed
elaborate nel corso degli anni con le proprie famiglie e con relazioni
di coppia del passato.

La formazione stessa della coppia e l’unione da cui originerà la
famiglia rappresenta di per se un evento critico.

I partner che attuano una convivenza (matrimonio o di fatto) si
trovano a dover affrontare tre aspetti: la costruzione di un’identità
di coppia, la cura delle differenze e la differenziazione e lo
svincolo dalle famiglie d’origine.

Costruire un’identità di coppia significa arrivare a definire un
“noi”. Attraverso una relazione rispondente e alla costruzione di un
progetto comune. Attraverso la comunicazione ogni coppia comincia a
definire delle funzioni, agli spazi e alle distanze rispetto a bisogni
di intimità e di libertà personale.

Ogni partner è inoltre impegnato nella costruzione di un legame basato
sull’empatia che favorisce la fiducia nell’altro. Il coinvolgimento
empatico prevede che ognuno si impegni per il bene dell’altro.

Attraverso il dialogo si esplorano e si costruiscono regole di
condotta condivise, il confronto e la negoziazione sui vari aspetti
della vita, ci si accorda su diritti, doveri e spazi condivisi.

##### SECONDA FASE: LA FAMIGLIA CON BAMBINI

Un passaggio fondamentale nella famiglia è il passaggio dall’essere
solo coniugi all’essere anche genitori.

Con la nascita di un figlio il sistema cambia in modo permanente e
definitivo. Si stabilisce un legame, che a differenza di quello
coniugale, non è dissolubile.

La nascita di un figlio rappresenta un evento critico che modifica in
larga misura l’equilibrio precedentemente raggiunto dalla coppia che
deve ridefinirsi rispetto al ruolo genitoriale, costruire nuovi ruoli
e funzioni, rinegoziare ruoli e posizioni rispetto alla famiglia
d’origine.

Nasce un sottosistema nuovo (genitoriale) e i membri della coppia sono
chiamati a stabilire dei chiari confini con il sistema-coppia.

All’interno della famiglia si possono evidenziare tre sottosistemi: il
sistema coniugale (relazione fra coniugi che si scambiano sostegno
emotivo-affettivo-coniugale), il sistema genitoriale (le funzioni
parentali che prevedono l’accudimento e l’impegno nel crescere i
figli) e il sistema dei fratelli (relazione fra pari, i fratelli, con
gli aspetti della negoziazione, della competizione e della
cooperazione).

Ogni sottosistema ha funzioni proprie e specifiche richieste per i
suoi membri.

Ogni componente della famiglia può fare contemporaneamente parte di
più di un sottosistema. Una persona può essere contemporaneamente
figlio, fratello, nipote, genitore, coniuge

A seconda dello status, in ogni sottosistema un individuo l’individuo
ha differenti gradi di potere e funzioni diverse. Può così
sperimentare le capacità interpersonali a diversi livelli.

I sottosistemi sono delineati fra loro da confini costituiti dalle
regole che definiscono chi e come partecipa alle relazioni famigliari.

Se i confini sono sufficientemente chiari, la famiglia ha un buon
funzionamento poiché i membri possono fare riferimento a delle regole
di funzionamento chiare che limitano le interferenze da un
sottosistema ad un altro.

Ad esempio la capacità dei due coniugi di adattarsi in modo
complementare richiede la non interferenza dei suoceri e dei figli,
oppure la capacità di negoziare coi coetanei, appresa nella relazione
fra i fratelli, richiede la non interferenza dei genitori.

Ciascun sottosistema emozionale deve avere il suo spazio. Nelle coppie
disfunzionali è frequente che i conflitti di tipo coniugale invadano
l’ambito della relazione genitoriale, ove spesso il figlio si coalizza
con uno dei genitori e può essere da entrambi triangolato oppure il
figlio può diventare il soggetto su cui si proietta e devia il
conflitto coniugale.

Nei casi in cui la coppia arriva alla separazione questi meccanismi
che coinvolgono i bambini nelle dinamiche di coppia a volte si
amplificano.

I genitori invece, come dovere genitoriale hanno il compito di
confrontarsi sul tipo di atteggiamento educativo che si terrà col
figlio mediando fra i diversi stili che ognuno porta con sé dalle
proprie famiglie d’origine. La reciproca cura delle differenze dovrà
intensificarsi fra i coniugi, considerando bisogni e interessi del
proprio bambino.

Prendersi cura significa offrire affetto e protezione per consentire
al bambino di sviluppare la fiducia e la stima verso se e verso gli
altri. Fondamentale è l’introduzione delle regole che aiutano a
discriminare e a distinguere tra ciò che è bene e ciò che male.

Se la relazione di coppia è ben definita dalle sue regole, questo
processo è possibile nel bambino altrimenti si avranno delle
commistioni che generano confusione nello sviluppo dell’identità del
bambino.

I genitori devono fornire un valido modello di attaccamento affettivo
ed educativo, devono aiutare i figli a confrontarsi con la realtà
sociale ed extrafamiliare (ad esempio la scuola). A volte, capita che
nei figli di genitori separati o in fase di separazione manifestino la
paura di andare a scuola a causa di un timore di abbandono che la
situazione di separazione dei genitori può evocare dentro di loro.

Parallelamente, così come i genitori hanno dei compiti rispetto ai
figli, anche i figli hanno dei loro compiti di sviluppo
specifici. Essi devono lavorare per costruire una relazione sempre più
paritaria con i genitori confrontandosi con l’iniziale e fisiologico
vissuto dei genitori visti come onnipotenti e perfetti e in grado di
soddisfare ogni loro bisogno.

##### TERZA FASE: LA FAMIGLIA CON ADOLESCENTI

In questa fase, il compito evolutivo fondamentale dei membri delle
diverse generazioni riguarda il naturale processo di separazione
reciproca.

Il rimodellamento della personalità dell’adolescente, tipico della sua
età, diventa lo stimolo per la trasformazione della famiglia nel suo
insieme.

L’evento critico è rappresentato dall’adolescenza dei figli e dalla
crisi di mezza età dei genitori.

In questa fase la coppia di partner è impegnata nel ridefinire la
relazione coniugale affrontando l’aumento dei momenti di solitudine ed
intimità della coppia. Inoltre bisogna costruire nuovi spazi sociali
come singoli e come coppia di genitori, adeguare la relazione con il
partner alla nuova situazione e favorire lo svincolo progressivo del
figlio.

Su un atro versante la coppia è impegnata nel favorire una guida
sicura, in quanto figli, ai propri genitori anziani e accettare il
processo di invecchiamento della generazione predente e del proprio.

I coniugi devono iniziare a fare i conti con le modificazioni di
carattere involutive del proprio corpo dovuta alla comparsa dei segni
dell’invecchiamento. Le donne dovranno saper accettare la menopausa,
collegata alla fine della possibilità di avere figli, gli uomini
dovranno accettare l’andropausa, segno della diminuzione della potenza
sessuale.

La difficoltà nell’accettazione di questi aspetti potrebbe portare la
coppia a mettere in difficoltà lo sviluppo dei figli cercando di
rivivere la propria giovinezza attraverso di loro o addirittura
costruendo legami coniugali nuovi con persone molto più giovani.

È un momento di crisi d’identità vera e propria che implica un
processo di ristrutturazione che è importante vivere ed elaborare bene
nella coppia che deve ridefinire la relazione coniugale reinvestendo
in essa sia attraverso la valorizzazione della propria attività
professionale sia maturando nuovi interessi sociali e culturali
(individuali e di coppia).

È fondamentale che la coppia arrivi ad un’elaborazione condivisa di
questi sentimenti, che i partner arrivino insieme a ridefinire la
relazione coniugale e investire in essa.

In caso contrario si rischia una crisi del rapporto che potrebbe
portare ad una rottura. Occorre saper ricostruire un proprio spazio e
nuovi ruoli nella coppia, nella famiglia e nell’ambiente esterno.

Per quanto riguarda la relazione coi figli deve essere rielaborata al
fine di conseguire il processo di reciproca separazione, favorire la
costruzione per il figlio di una propria identità separata,
sviluppando un atteggiamento di protezione flessibile, aumentare la
duttilità dei confini familiari per permettere e favorire il
cambiamento e lo svincolo progressivo del ragazzo rimando una giuda
sicura, in particolare nei momenti di difficoltà.

I genitori dovranno continuare a orientare il figlio nella scelte in
modo “personalizzato” quando questo ne porterà il bisogno affinchè
possa trovare la propria strada senza chiedergli, più o meno
esplicitamente, di realizzare i sogni dei genitori invece del proprio
futuro.

Il ragazzo deve sentire di poter contare sempre sui propri genitori e
nel contempo non deve sentire ostacolata la sua autonomia e la ricerca
della sua nuova identità.

I ragazzi, per contro, sono impegnati a mantenere vivo il colloquio e
il rapporto coi genitori e accettare il loro processo di
invecchiamento.

##### QUARTA FASE: LA FAMIGLIA CON FIGLI ADULTI

Come i figli devono costruire una propria vita affettiva e lavorativa
autonoma e indipendente, anche i genitori devono accettare la
separazione dai propri figli e accompagnare la loro crescente
indipendenza ridefinendo le relazioni genitori-figli nella direzione
di un rapporto alla pari.

In caso contrario i genitori potrebbero soffrire della “Sindrome del
nido vuoto” non riuscendo ad accettare la solitudine che comporta la
dipartita del figlio (fisica e/o mentale) dalla casa natia e il loro
naturale processo di crescita.

È anche il momento in cui i coniugi si riguardano negli occhi e
riscoprono la loro identità di partner.

Questa è una fase critica nella quale diverse coppie non ritrovano più
l’immagine della persona che avevano in mente, inoltre, vecchi
problemi di coppia non risolti tornano prepotentemente alla ribalta in
questo spazio che si viene a creare.

I partner hanno il compito di reinvestire l’amore e le risorse nella
relazione coniugale, di prepararsi all’uscita di casa dei figli, di
stabilire una relazione con questi non più secondo una dinamica di
adulto-bambino ma una di adulto-adulto, paritetica e interdipendente e
di prendersi cura della generazione anziana.

I genitori devo tollerare possibili sentimenti di abbandono e vuoto
derivanti dalla separazione.

L’uscita di casa del figlio è strettamente legata al processo di
svincolo adolescenziale. Se tale processo fosse stato bloccato in
quella fase, l’uscita di casa del figlio potrebbe creare un grande
squilibrio nella coppia, tanto da provocare una crisi o addirittura
una rottura del rapporto.

Se i figli sentiranno che i loro genitori non potranno farcela da
soli, probabilmente rimanderanno il momento della loro indipendenza,
avendo poi difficoltà e realizzarsi come adulti e rimanendo incastrati
nelle dinamiche della coppia genitoriale.

Sull’altro fronte, la coppia è impegnata nella cura dei genitori che
diventano anziani. La coppia si trova tra le richieste dei figli che
stanno diventando grandi ma che ancora necessitano e chiedono le loro
cure e la loro guida, e allo stesso tempo i genitori anziani si
aspettano attenzioni, cure, compagnia, aiuto, in una relazione che
fini a poco prima era invece una relazione paritaria.

##### QUNTA FASE: LA FAMIGLIA NELL’ETA’ ANZIANA

Se le fasi precedenti vengono superate e la coppia riesce a
riadattarsi e trovare un nuovo equilibri, raggiungendo un maggior
senso di intimità e solidarietà, questa fase può essere molto intensa.

Entrambi i coniugi sono meno impegnati professionalmente e nel loro
ruolo di genitori e possono quindi viversi con più tempo e serenità il
loro rapporto di coppia.

Gli eventi che possono risultare critici in questa fase sono il
pensionamento, il cambio di ruolo determinato dal fatto di diventare
eventualmente nonni, le malattie e il timore della morte propria o del
coniuge.
