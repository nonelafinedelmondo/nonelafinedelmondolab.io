+++
fragment = "content"
weight = 100
date = "2020-05-14"
title = "No, avere un brutto carattere non è un merito"
background = "light"
+++

Scriverò, come faccio spesso, un post impopolare e ispirato al solito
fiorire di post, articoli e meme di psicologia fai da che magnificano
l’avere un brutto carattere.

Sembra che essere una ‘persona difficile’ sia il massimo a cui si
possa aspirare. In contrapposizione con le persone ‘semplici’ che
sarebbero povere di spirito e dall’intelletto elementare.

Ancora una volta, non siamo tutti psicologi (ancora una volta cito
Sara Collicelli) e cercare una letteratura pseudoscientifica che
trasformi i nostri limiti in meriti non è una buona idea.

Avere un brutto carattere non è un merito.

Vivere conflitti frequenti, non avere chi ci comprende, accorgersi che
gli amici ci evitano non significa che siamo troppo superiori agli
altri per essere compresi, e neanche che attorno a noi ci sono persone
così pigre da non essere in grado di frequentare qualcuno così
arzigogolato.

Non vuol dire che noi siamo una rara perla in un mare di palline di
plastica, che solo un vero estimatore delle perle sopporterà mille
traversie per trovarci e amarci come meritiamo.

È tutto molto più banale, i rapporti umani si solidificano quando si
sta bene assieme.

Da mediatrice familiare ho imparato questa banale verità: durano le
cose belle, durano i rapporti in cui ci facciamo carico del benessere
dell’altro e in cui semplifichiamo le cose.

Durano i rapporti in cui riconosciamo i nostri difetti, in cui
semplifichiamo le nostre complicazioni, in cui favoriamo la
comunicazione serena e non violenta.

Se pertanto vivete frequentemente rapporti conflittuali e conseguenti
abbandoni, piuttosto che auto convincervi di essere una persona
‘complicata’, dando al termine complicato una accezione
incredibilmente positiva, provate a ragionare su alcune questioni:

1. Starmi accanto è piacevole?
2. lavoro sul mio modo di comunicare?
3. esprimo pareri ipercritici e  arbitrari?
4. condivido le mie sventure e tengo per me le mie gioie?
5. quando frequento qualcuno cerco di mettere in evidenza le mie
   esigenze tenendo in eguale conto le esigenze degli altri?
6. mi ricordo dei bisogni dell’altro o mi aspetto che i miei siano al
   centro del rapporto?

Una volta che avrete analizzato il vostro modo di porvi potrete darvi
come obiettivo quello di smettere di essere persone ‘difficili’,
riconoscere che essere ‘difficili’ è un limite, e lavorare su vuoi
stessi per diventare persone piacevoli.
