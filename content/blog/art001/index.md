+++
fragment = "content"
weight = 100
date = "2020-05-13"
title = "Nuovi partner e figli, come comportarsi?"
background = "light"
+++

Uno degli argomenti principali che vengono affrontati durante una
mediazione familiare e quello relativo ai potenziali nuovi partner dei
futuri ex coniugi.

Tendenzialmente c’è una diffusa paura che le nuove relazioni possano
in qualche modo turbare i figli.

Proviamo quindi a stilare un breve decalogo che ci guidi a risolvere
questa criticità.

1. Avere dei nuovi partner e del tutto naturale, pertanto è sbagliato
   ed inopportuno osteggiare chi ne abbia trovato uno.
   
   Inoltre mostrare ai figli che dopo la fine di un matrimonio si
   possano instaurare relazioni serene aiuta a dare ai figli una
   prospettiva meno negativa sull’amore.

2. Nell’accordo di separazione non c’è spazio per regolamentare i
   comportamenti dell’ex coniuge rispetto ad un potenziale futuro
   partner. Ogni potenziale richiesta di restringere il diritto a
   parlare nuove relazioni ai figli è nullo.

3. I nostri figli non sono i nostri confidenti.

   Bisogna evitare di coinvolgerli nei rapporti in fieri e di
   raccontare loro ogni turbamento emotivo.

4. La regola della doppia D

   All’inizio di una relazione bisogna essere diligenti e discreti,
   avere ogni attenzione necessaria per non affrettare i tempi di un
   coinvolgimento quando il rapporto è ancora in fase embrionale.

5. Con i figli non bisogna "essere omertosi"

   Quando abbiamo una relazione continuativa, questa si riflette
   inevitabilmente nella vita quotidiana: una telefonata in più, il
   cellulare più spesso tra le mani e i nostri figli si accorgeranno
   che c’è qualche novità. In quel caso è opportuno essere chiari,
   spiegare cosa sta succedendo, senza chiedere la loro “benedizione”
   e senza sottoporre nulla al loro giudizio.

6. No a "è un amico speciale"

   Rispettiamo l’intelligenza dei nostri figli. Un "amico" che passa
   tutto il tempo libero con il loro genitore ovviamente non è un
   amico. Questo inganno non favorisce la chiarezza del rapporto e
   potrà condizionarne il futuro.

7. Il momento delle presentazioni

   Informare l’altro genitore prima di presentare il nuovo partner ai
   figli. Notate bene, decidere quando sarà il momento giusto per
   questo passo dipende solo da voi. E’ una scelta che non dovete
   concordare e della quale vi assumete per intero la responsabilità.

8. No a promesse di amore eterno

   Quando presenterete il vostro nuovo partner non è opportuno
   promettere che sarà un amore definitivo e durativo. Puntate
   l’attenzione sulle vostre speranze e sull’impegno che contate di
   metterci, senza negare le difficoltà nel costruire una relazione
   duratura.

9. No a confronti

   Non c’è spazio, nel rapporto con i figli, per una valutazione
   comparativa tra il nuovo rapporto e quello precedente. Quindi vanno
   tassativamente evitate frasi che suonino come “questo è il rapporto
   giusto…”

10. No a famiglie allargate a tutti i costi

    Se tra i vostri figli e il vostro partner (ed eventualmente i suoi
    figli) ci fosse un enorme feeling, nulla impedisce la costruzione
    di una famiglia allargata.

    Ma vanno tenute in enorme riguardo le sensibilità dei vostri
    figli, che non è detto desiderino palesare il disappunto davanti
    ad una eventuale costruzione di una famiglia xxl. Pertanto abbiate
    cura di cogliere ogni sfumatura del loro modo di comunicarvi se
    questa eventualità possa essere a loro gradita.

11. Accordati sul “dopo”

    E se la relazione dovesse finire? Accordarsi con il nuovo partner
    sulla gestione della fine della relazione, per ciò che attiene al
    rapporto con i figli dell’altro, è fondamentale per non far vivere
    loro un abbandono non necessario.
