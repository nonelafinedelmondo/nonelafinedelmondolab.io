+++
title = "Avv. Sergio Spina"
weight = 10
date = "2020-02-05"

#[asset]
  #image = "spina.png"
+++

<div style="text-align: justify;">

Laureato in Giurisprudenza presso l’Università degli Studi di Bari.

Si è formato come avvocato presso i principali Studi legali penali e civili di Foggia,

Avvocato dal 1998, ha lavorato come civilista in un proprio Studio, dove ha affrontato tutte le tematiche tipiche: contrattualistica, diritto di famiglia, responsabilità civile, condominio degli edifici, esecuzioni mobiliari e immobiliari, lavoro e previdenza.

Dal 2001 ha focalizzato la propria attività sul tema della responsabilità medica, nella quale ha potuto accumulare conoscenze ed esperienze di particolare importanza.

Nel 2019 realizza il progetto di ampliare la propria attività, aprendo uno studio a Torino.

</div>

<div style="text-align: left;">

- Indirizzi:
  + Studio di Foggia: Via Paolo Telesforo 27
  + Studio di Torino: Corso Rosselli 68
- Telefono: +39 327 298 1826
- Email: sergio.am.spina@gmail.com

</div>
